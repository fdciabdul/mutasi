const ScraperBank = require("../lib/scraper.class");
var format = require("date-format");
module.exports = async function (app) {
  app.get("/", function (req, res) {
  res.render("index" ,{
    inputdate: format.asString("dd-MM-yyyy", new Date()),
  });
  });
  app.post("/check/", async function (req, res) {
    
    switch (req.body.bank) {
      case "bri":
       
        var scraper = new ScraperBank(req.body.user, req.body.pass  , {
          headless : false
        });

        
        scraper.getBRI(req.body.norek).then(function (data ,err) {
          if(err){
            res.send(err)
          }
          res.send(data)
        })
   
        break;
        
      case "bca":
        var scraper = new ScraperBank(req.body.user, req.body.pass  , {
          headless : false
        });
        const date = req.body.date_begin
        const [day, month, year] = date.split('-')
        const date1 = req.body.date_end
        const [day1, month1, year1] = date1.split('-')
        
        scraper.getBCA(
          day,
          month,
          day1,
          month1
        ).then(function (data ,err) {
          if(err){
            res.send(err)
          }
          res.send(data)
        })
   
        break;
      case "bni":
        var result = await scraper.getBNI();
        res.json(result);
        break;
      case "danamon":
        var result = await scraper.getDanamon();
        res.json(result);
        break;
      case "mandiri":
        var result = await scraper.getBNI();
        res.json(result);
        break;
      default:
        break;
    }
  });
};
